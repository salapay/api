# Salapay API definition

## About
This repository hosts the definition of the SalaPay API in the OpenAPI 3 
specification.

For the implementation referee to this repository: [SalaPay Gitlab Repository](https://gitlab.com/salapay/salapay) 

## Usage

We use the [OpenAPI](https://openapi-generator.tech/) generator to generate the server side stubs.

### How to generate the source files:

Install the generator, see [here](https://openapi-generator.tech/docs/installation): 
```bash
$ npm install @openapitools/openapi-generator-cli -g
```

Generate the sources, see [usage](https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/spring.md):
```bash
$ npx openapi-generator generate -g spring -i salapay_swagger_openapi_definition.yaml -c config.json -o ./generated/
```

