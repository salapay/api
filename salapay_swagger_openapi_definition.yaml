openapi: 3.0.0
info:
  description: |
    This file defineds the SalaPay API.\
    [Find out more about the API on the project repository](http://gitlab.com/salapay/server)
  version: "1.0.0"
  title: SalaPay API
  termsOfService: 'http://swagger.io/terms/'
  contact:
    email: SalaPay@wagnergerald.com
  license:
    name: GNU AGPLv3
    url: 'https://www.gnu.org/licenses/agpl-3.0.html'
servers:
  # Added by API Auto Mocking Plugin
  - description: LIVE Test endpoint
    url: https://salapay.wagnergerald.com/api/1.0.0
  - description: Development endpoint
    url: http://localhost:8080/api/1.0.0
tags:
  - name: payment
    description: Defines a paymant API for cryptocurrencies
    externalDocs:
      description: Find out more about the Salapy API
      url: 'https://gitlab.com/salapay/server/-/blob/master/README.md'

paths:
  /user:
    post:
      security:
        - basicAuth: []
      tags:
        - user
      summary: Create user
      description: Create a user with given body data.
      operationId: createUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateUser'
            example:
              username: live-test
              firstName: live
              lastName: test
              email: test1@example.com
              password: test
              phone: +43123456123456
              transactionLimit: 10000.0
              description: some text
        description: Created user object
        required: true
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '500':
          description: Unsuccessful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /user/{userId}:
    get:
      security:
        - basicAuth: []
      tags:
        - user
      summary: Retreive an user object
      description: Retrieve an user object by passing the id
      operationId: getUser
      parameters:
        - name: "userId"
          in: path
          description: "User id"
          required: true
          schema:
            type: integer
            format: int64
          examples:
            justCreated:
              value: 14
              summary: User you may have just created with the create endpoint
            gerald:
              value: 5
              summary: User with all wallets
            test:
              value: 9
              summary: Another user with all wallets
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '500':
          description: Unsuccessful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /wallet/create:
    post:
      security:
        - basicAuth: []
      tags:
        - wallet
      summary: Creates an currency account
      description: Create a new wallet for a user with a given currency
      operationId: createUserWallet
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Wallet'
        '500':
          description: Unsuccessful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateWalletRequest'
            example:
              userId: 14
              currency: Ethereum
        description: Created user object
        required: true
  /wallet/{walletId}:
    get:
      security:
        - basicAuth: []
      tags:
        - wallet
      summary: Get wallet
      description: Retrieve a wallet by providing the walletId
      operationId: getWallet
      parameters:
        - name: "walletId"
          in: path
          description: "Wallet id"
          required: true
          schema:
            type: integer
            format: int64
          examples:
            walletId:
              value: 15
              summary: 'Wallet you may have just created'
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Wallet'
        '500':
          description: Unsuccessful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /transaction/submit:
    post:
      security:
        - basicAuth: []
      tags:
        - transaction
      summary: Creates an new transaction
      description: Multiple status values can be provided with comma separated strings
      operationId: submitTransaction
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/TransactionRequest'
            example:
              amount: 1.0
              currency: Ethereum
              sourceUserId: 5
              targetUserId: 14
        description: Created a transaction
        required: true
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Transaction'
        '500':
          description: Unsuccessful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /transaction/{transactionId}:
    get:
      security:
        - basicAuth: []
      tags:
        - transaction
      summary: Status of the transaction
      description: Multiple status values can be provided with comma separated strings
      operationId: getTransactionGetStatus
      parameters:
        - name: "transactionId"
          in: path
          description: "transactionId"
          required: true
          schema:
            type: string
            format: uuid
          examples:
            transactionId:
               summary: (UU)ID from an transaction
               value: <UUID-from-placeholder>
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Transaction'
        '500':
          description: Unsuccessful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /transaction/list/{userId}:
    get:
      security:
        - basicAuth: []
      tags:
        - transaction
      summary: List all transactions of a given user
      description: Multiple status values can be provided with comma separated strings
      operationId: getUserTransactions
      parameters:
        - name: "userId"
          in: path
          description: "userId"
          required: true
          schema:
            type: integer
            format: int64
          examples:
            userId:
              value: 14
              summary: test
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Transaction'
        '500':
          description: Unsuccessful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
externalDocs:
  description: Find out more about Swagger
  url: 'http://swagger.io'
components:
  schemas:
    UserStatus:
      type: string
      enum:
        - Active
        - Deactivated
    TransactionState:
      type: string
      enum:
        - REJECTED
        - ACCEPTED_PENDING
        - ACCEPTED_PROCESSED
    Currency:
      type: string
      enum:
        - Ethereum
        - Bitcoin
        - BitcoinCash
    CreateUser:
      type: object
      properties:
        username:
          type: string
        firstName:
          type: string
        lastName:
          type: string
        email:
          type: string
        password:
          type: string
        phone:
          type: string
        transactionLimit:
          type: number
          format: double
        description:
          type: string
    User:
      type: object
      properties:
        id:
          type: integer
          format: int64
        description:
          type: string
        email:
          type: string
        username:
          type: string
        firstName:
          type: string
        lastName:
          type: string
        phone:
          type: string
        transactionLimit:
          type: number
          format: double
        userStatus:
          $ref: '#/components/schemas/UserStatus'
        wallets:
          type: array
          items:
            $ref: '#/components/schemas/Wallet'
    Wallet:
      type: object
      properties:
        id:
          type: integer
          format: int64
        currency:
          $ref: '#/components/schemas/Currency'
        balance:
          type: number
          format: double
    CreateWalletRequest:
      type: object
      properties:
        currency:
          $ref: '#/components/schemas/Currency'
        userId:
          type: integer
          format: int64
    TransactionRequest:
      type: object
      properties:
        currency:
          $ref: '#/components/schemas/Currency'
        amount:
          type: number
          format: double
        sourceUserId:
          type: integer
          format: int64
        targetUserId:
          type: integer
          format: int64
    Transaction:
      type: object
      properties:
        identifier:
          type: string
          format: uuid
        amount:
          type: number
          format: double
        currency:
          $ref: '#/components/schemas/Currency'
        sourceUserId:
          type: integer
          format: int64
        targetUserId:
          type: integer
          format: int64
        timestampCreated:
          type: string
          format: date-time
        timestampProcessed:
          type: string
          format: date-time
        state:
          $ref: '#/components/schemas/TransactionState'
    ErrorResponse:
      type: object
      properties:
        description:
          type: string
  securitySchemes:
    basicAuth:     # <-- arbitrary name for the security scheme
      type: http
      scheme: basic
security:
  - basicAuth: []